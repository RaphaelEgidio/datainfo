import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './app/home/home.component';
import { ExternalUserComponent } from './app/user/external-user.component';
import { ExternalUserIncludeComponent } from './app/user-include/external-user-include.component'
 
const appRoutes: Routes = [
    { path: 'home',                    component: HomeComponent },
    { path: '',                        component: HomeComponent },
    { path: 'user',                    component: ExternalUserComponent },
    { path: 'user-include',            component: ExternalUserIncludeComponent },
    { path: 'user-include/:cpf',       component: ExternalUserIncludeComponent }
];
 
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);