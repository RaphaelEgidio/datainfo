import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';

import { ExternalUserFunctionService } from '../service/external-user-function.service';
import { ExternalUserService } from '../service/external-user.service';

import { ExternalUser } from '../service/external-user';

@Component({
  selector: 'external-user-list',
  templateUrl: './external-user.component.html',
  styleUrls: ['./external-user.component.css']
})
export class ExternalUserComponent implements OnInit {

  private externalUsers:ExternalUser[] = new Array();
  private externalUser:ExternalUser = new ExternalUser('A');

  constructor(
    private externalUserFunctionService: ExternalUserFunctionService,
    private externalUserService: ExternalUserService,
    private router: Router
  ) {}
  
  ngOnInit() {
    this.findAll();

    this.externalUserFunctionService.findAll().subscribe(
      response => {
        console.log('Response: ', response);
      }
    );
  }

  findAll():void {
    this.externalUserService.findAll(this.externalUser).subscribe(
      response => {
        this.externalUsers = response;
      }
    );
  }

  edit(cpf:string):void {
    this.router.navigate(['/user-include', cpf]);
  }

  include():void {
    this.router.navigate(['/user-include']);
  }

  enableOrDisable(cpf:string):void {
    this.externalUserService.enableOrDisable(cpf).subscribe(
      response => {
        console.log(response);
      }
    );
  }

  remove(cpf:string):void {
    this.externalUserService.remove(cpf).subscribe(
      response => {
        this.findAll();
      }
    );
  }

}
