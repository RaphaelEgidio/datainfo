import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { ExternalUser } from '../service/external-user';
import { ConfigurationService } from '../service/config.service';

@Injectable()
export class ExternalUserService {

    private urlService:string = '';
    private headers:Headers;
    private options:RequestOptions;

    constructor(private http: Http, private configService: ConfigurationService) {
        this.headers = new Headers({ 'Content-Type': 'application/json;charset=UTF-8' });                
        this.options = new RequestOptions({ headers: this.headers });
        this.urlService = configService.getUrl() + '/external/user';
    }

    findOne(cpf:number) {
        return this.http.get(this.urlService + '/find-one/' + cpf).pipe(map(res => res.json()));
    }

    findAll(externalUser:ExternalUser) {
        return this.http.post((this.urlService + '/find-all'), JSON.stringify(externalUser), this.options).pipe(map(res => res.json()));
    }

    remove(cpf:string) {
        return this.http.delete((this.urlService + '/remove/' + cpf));
    }

    enableOrDisable(cpf:string) {
        return this.http.get((this.urlService + '/remove/' + cpf));
    }

    save(externalUser:ExternalUser) {
        return this.http.post((this.urlService + '/save'), JSON.stringify(externalUser), this.options).pipe(map(res => res.json()));
    }

}
