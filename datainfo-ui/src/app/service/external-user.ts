import { ExternalUserFunction } from '../service/external-user-function'

export class ExternalUser {
    cpf:string;
    name:string;
	email:string;
	situation:string;
	accessProfile:number;
	phone:string;
	functionCode:number;
	update:boolean;
	externalUserFunctionDto:ExternalUserFunction;

	constructor(situation:string) {
		this.situation = situation;
	}
}