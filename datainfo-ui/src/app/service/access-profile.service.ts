import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';

import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { ConfigurationService } from '../service/config.service';

@Injectable()
export class AccessProfileService {

    private urlService:string = '';
    private headers:Headers;
    private options:RequestOptions;

    constructor(private http: Http, private configService: ConfigurationService) {
        this.headers = new Headers({ 'Content-Type': 'application/json;charset=UTF-8' });                
        this.options = new RequestOptions({ headers: this.headers });
        this.urlService = configService.getUrl() + '/access-profile';
    }

    findAll() {
        return this.http.get((this.urlService + '/find-all')).pipe(map(res => res.json()));
    }

}
