export class ConfigurationService {
    private url:string;

    constructor() {
        this.url = 'http://localhost:8086/datainfo';
    }

    getUrl(): string {
        return this.url;
    }
}