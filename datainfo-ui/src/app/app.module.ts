import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ExternalUserComponent } from './user/external-user.component';
import { HomeComponent } from './home/home.component';
import { ExternalUserIncludeComponent } from './user-include/external-user-include.component';

import { routing } from './../app.routes';

import { ConfigurationService } from '../app/service/config.service';
import { ExternalUserFunctionService } from '../app/service/external-user-function.service';
import { ExternalUserService } from '../app/service/external-user.service';
import { AccessProfileService } from '../app/service/access-profile.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ExternalUserComponent,
    HomeComponent,
    ExternalUserIncludeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    FormsModule 
  ],
  providers: [
    ConfigurationService,
    ExternalUserFunctionService,
    ExternalUserService,
    AccessProfileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
