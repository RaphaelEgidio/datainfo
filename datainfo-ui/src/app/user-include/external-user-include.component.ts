import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { ExternalUserFunctionService } from '../service/external-user-function.service';
import { ExternalUserService } from '../service/external-user.service';
import { ExternalUser } from '../service/external-user';
import { AccessProfile } from '../service/access-profile';
import { ExternalUserFunction } from '../service/external-user-function';
import { AccessProfileService } from '../service/access-profile.service';

@Component({
  selector: 'external-user-include',
  templateUrl: './external-user-include.component.html',
  styleUrls: ['./external-user-include.component.css']
})
export class ExternalUserIncludeComponent implements OnInit {

  private accessProfiles: AccessProfile[] = new Array();
  private externalUserFunctions: ExternalUserFunction[] = new Array();
  private externalUser: ExternalUser = new ExternalUser('A');
  private titlePage: string;
  private update: boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private externalUserFunctionService: ExternalUserFunctionService,
    private externalUserService: ExternalUserService,
    private accessProfileService: AccessProfileService
  ) {}
  
  ngOnInit() {

    this.activatedRoute.params.subscribe(parametro => {
      if(parametro["cpf"] == undefined){
        this.titlePage = "Incluir Novo Usuário";
        this.update = false;
      } else{
        this.update = true;
        this.titlePage = "Editar Usuário";
        this.externalUserService.findOne(parametro["cpf"]).subscribe(res => this.externalUser = res)
      }
    }); 

    this.accessProfileService.findAll().subscribe(
      response => {
        this.accessProfiles = response;
      }
    );

    this.externalUserFunctionService.findAll().subscribe(
      response => {
        this.externalUserFunctions = response;
      }
    );
  }

  save():void {
    this.externalUser.update = this.update;
    // Não tive tempo de fazer os "SELECT" da tela funcionar portanto estou passando os valores estaticos quando UPDATE for false.
    if (!this.update) {
      this.externalUser.situation = "A";
      this.externalUser.accessProfile = 0;
      this.externalUser.externalUserFunctionDto = this.externalUserFunctions[0];
    }

    this.externalUserService.save(this.externalUser).subscribe(response => {
      this.router.navigate(['/user']);
    });
  }

  cancel():void {
    this.router.navigate(['/user']);
  }

}
