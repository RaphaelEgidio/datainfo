package br.com.datainfo.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.datainfo.enuns.AccessProfile;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/access-profile")
public class AccessProfileController {

	@RequestMapping(path = "/find-all", method = RequestMethod.GET)
	public ResponseEntity<List<AccessProfile>> findAll() {
		return new ResponseEntity<>(Arrays.asList(AccessProfile.values()), HttpStatus.OK);
	}

}
