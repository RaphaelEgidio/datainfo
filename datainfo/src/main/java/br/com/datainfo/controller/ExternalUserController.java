package br.com.datainfo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.datainfo.DataInfoException;
import br.com.datainfo.dto.ExternalUserDto;
import br.com.datainfo.entity.ExternalUser;
import br.com.datainfo.service.ExternalUserService;
import br.com.datainfo.utils.ConverterUtils;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/external/user")
public class ExternalUserController implements BasicCrud<ExternalUserDto> {

	@Autowired
	private ExternalUserService externalUserService;

	@RequestMapping(path = "/save", method = RequestMethod.POST)
	public ResponseEntity<ExternalUserDto> save(@RequestBody final ExternalUserDto externalUserDto)
			throws DataInfoException {

		final ExternalUser externalUser = this.externalUserService.saveOrUpdate(externalUserDto.toEntity(), externalUserDto.isUpdate());

		return new ResponseEntity<>(ConverterUtils.entityToDto(externalUser), HttpStatus.OK);
	}

	@RequestMapping(path = "/find-one/{id}", method = RequestMethod.GET)
	public ResponseEntity<ExternalUserDto> findOne(@PathVariable final String id) {
		return new ResponseEntity<>(this.externalUserService.findOne(id).toDto(), HttpStatus.OK);
	}

	@RequestMapping(path = "/find-all", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<ExternalUserDto>> findAll(@RequestBody final ExternalUserDto externalUser) {
		return new ResponseEntity<>(
				ConverterUtils.entityToDto(this.externalUserService.findAll(externalUser.toEntity())), HttpStatus.OK);
	}

	@RequestMapping(path = "/remove/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> remove(@PathVariable final String id) {
		this.externalUserService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(path = "/enableOrDisable/{id}", method = RequestMethod.GET)
	public ResponseEntity<Void> enableOrDisable(@PathVariable final String id) {
		this.externalUserService.enableOrDisable(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
