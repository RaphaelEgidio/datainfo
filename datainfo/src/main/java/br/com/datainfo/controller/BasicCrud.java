package br.com.datainfo.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import br.com.datainfo.DataInfoException;

public interface BasicCrud<T> {

	public ResponseEntity<T> save(final T entity) throws DataInfoException;

	public ResponseEntity<T> findOne(final String id);

	public ResponseEntity<List<T>> findAll(final T entity);

	public ResponseEntity<Void> remove(final String id);

}
