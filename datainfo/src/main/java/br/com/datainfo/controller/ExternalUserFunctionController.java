package br.com.datainfo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.datainfo.entity.ExternalUserFunction;
import br.com.datainfo.repository.ExternalUserFunctionRepository;

@CrossOrigin(origins  = "http://localhost:4200")
@RestController
@RequestMapping("/external/user/function")
public class ExternalUserFunctionController {

	@Autowired
	private ExternalUserFunctionRepository externalUserFunctionRepository;

	@RequestMapping(path = "/find-one/{id}", method = RequestMethod.GET)
	public ResponseEntity<ExternalUserFunction> findOne(@PathVariable final Integer id) {
		final Optional<ExternalUserFunction> findById = this.externalUserFunctionRepository.findById(id);

		return new ResponseEntity<>(findById.isPresent() ? findById.get() : null, HttpStatus.OK);
	}

	@RequestMapping(path = "/find-all", method = RequestMethod.GET)
	public ResponseEntity<List<ExternalUserFunction>> findAll() {
		return new ResponseEntity<>(this.externalUserFunctionRepository.findAll(), HttpStatus.OK);
	}

}
