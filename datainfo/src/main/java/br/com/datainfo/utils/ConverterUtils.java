package br.com.datainfo.utils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import br.com.datainfo.dto.ExternalUserDto;
import br.com.datainfo.dto.ExternalUserFunctionDto;
import br.com.datainfo.entity.ExternalUser;
import br.com.datainfo.entity.ExternalUserFunction;
import br.com.datainfo.enuns.AccessProfile;
import br.com.datainfo.enuns.Situation;

public final class ConverterUtils {

	public static ExternalUser dtoToEntity(final ExternalUserDto dto) {
		final ExternalUser entity = new ExternalUser();

		entity.setCpf(dto.getCpf());
		entity.setEmail(dto.getEmail());
		entity.setName(dto.getName());
		entity.setPhone(dto.getPhone());
		entity.setAccessProfile(
				Objects.nonNull(dto.getAccessProfile()) ? AccessProfile.values()[dto.getAccessProfile()] : null);
		entity.setSituation(StringUtils.isNotBlank(dto.getSituation()) ? Situation.valueOf(dto.getSituation()) : null);

		if (Objects.nonNull(dto.getAccessProfile())) {
			entity.setExternalUserFunction(dtoToEntity(dto.getExternalUserFunctionDto()));
		}

		return entity;
	}

	public static ExternalUserDto entityToDto(final ExternalUser entity) {
		final ExternalUserDto dto = new ExternalUserDto();

		dto.setAccessProfile(entity.getAccessProfile().getOrdinal());
		dto.setCpf(entity.getCpf());
		dto.setEmail(entity.getEmail());
		dto.setName(entity.getName());
		dto.setPhone(entity.getPhone());

		if (Objects.nonNull(entity.getExternalUserFunction())) {
			dto.setExternalUserFunctionDto(entityToDto(entity.getExternalUserFunction()));
		}

		dto.setSituation(entity.getSituation().toString());

		return dto;
	}

	public static List<ExternalUserDto> entityToDto(final List<ExternalUser> list) {
		return list.stream().map(i -> entityToDto(i)).collect(Collectors.toList());
	}

	public static ExternalUserFunction dtoToEntity(final ExternalUserFunctionDto dto) {
		final ExternalUserFunction entity = new ExternalUserFunction();

		entity.setCode(dto.getCode());
		entity.setFunction(dto.getFunction());

		return entity;
	}

	public static ExternalUserFunctionDto entityToDto(final ExternalUserFunction entity) {
		final ExternalUserFunctionDto dto = new ExternalUserFunctionDto();

		dto.setCode(entity.getCode());
		dto.setFunction(entity.getFunction());

		return dto;
	}

}
