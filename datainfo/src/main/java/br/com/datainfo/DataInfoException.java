package br.com.datainfo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class DataInfoException extends Exception {

	private static final long serialVersionUID = 1L;

	public DataInfoException() {
		super();
	}

	public DataInfoException(final String message) {
		super(message);
	}

}
