package br.com.datainfo.service;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.datainfo.DataInfoException;
import br.com.datainfo.entity.ExternalUser;
import br.com.datainfo.enuns.Situation;
import br.com.datainfo.repository.ExternalUserRepository;

@Service
public class ExternalUserService {

	@Autowired
	private ExternalUserRepository externalUserRepository;

	public ExternalUser saveOrUpdate(final ExternalUser externalUser, final Boolean update) throws DataInfoException {
		validation(externalUser, update);
		return this.externalUserRepository.save(externalUser);
	}

	private void validation(final ExternalUser externalUser, final Boolean update) throws DataInfoException {
		if (Objects.isNull(externalUser)) {
			throw new DataInfoException("Usuário não pode ser vazio!");
		}
		if (StringUtils.isBlank(externalUser.getCpf())) {
			throw new DataInfoException("CPF não pode ser vazio!");
		}
		if (!update && userExists(externalUser.getCpf())) {
			throw new DataInfoException("Já existe um usuário cadastrado com esse CPF!");
		}
	}

	private Boolean userExists(final String cpf) {
		final ExternalUser externalUser = this.externalUserRepository.findByCpf(cpf);
		return Objects.nonNull(externalUser) ? Boolean.TRUE : Boolean.FALSE;
	}

	public ExternalUser findOne(final String id) {
		return this.externalUserRepository.getOne(id);
	}

	public List<ExternalUser> findAll(final ExternalUser externalUser) {
		return this.externalUserRepository.findAll();
	}

	public void enableOrDisable(final String id) {
		final ExternalUser externalUser = this.externalUserRepository.getOne(id);

		externalUser.setSituation(externalUser.getSituation().getStatus() ? Situation.I : Situation.A);

		this.externalUserRepository.save(externalUser);
	}

	public void remove(final String id) {
		final ExternalUser externalUser = this.externalUserRepository.getOne(id);
		this.externalUserRepository.delete(externalUser);
	}

}
