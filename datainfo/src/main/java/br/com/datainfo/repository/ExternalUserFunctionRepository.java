package br.com.datainfo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.datainfo.entity.ExternalUserFunction;

@Repository
public interface ExternalUserFunctionRepository extends JpaRepository<ExternalUserFunction, Integer> {

}
