package br.com.datainfo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.datainfo.entity.ExternalUser;

@Repository
public interface ExternalUserRepository extends JpaRepository<ExternalUser, String> {

	public ExternalUser findByCpf(final String cpf);

}
