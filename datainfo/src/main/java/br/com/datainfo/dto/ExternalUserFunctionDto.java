package br.com.datainfo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalUserFunctionDto {

	private Integer code;

	private String function;

}
