package br.com.datainfo.dto;

import br.com.datainfo.entity.ExternalUser;
import br.com.datainfo.utils.ConverterUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalUserDto {

	private String cpf;

	private String name;

	private String email;

	private String situation;

	private Integer accessProfile;

	private String phone;
	
	private boolean update;

	private ExternalUserFunctionDto externalUserFunctionDto;

	public ExternalUser toEntity() {
		return ConverterUtils.dtoToEntity(this);
	}

}
