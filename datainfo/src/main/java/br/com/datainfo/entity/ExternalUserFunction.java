package br.com.datainfo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "funcao_usuario_externo")
public class ExternalUserFunction implements Serializable {

	private static final long serialVersionUID = -1850369623589661342L;

	@Id
	@Column(name = "co_funcao", nullable = false)
	private Integer code;

	@Column(name = "no_funcao", nullable = false)
	private String function;

}
