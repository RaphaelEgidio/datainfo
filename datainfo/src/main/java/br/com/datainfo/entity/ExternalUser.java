package br.com.datainfo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.datainfo.dto.ExternalUserDto;
import br.com.datainfo.enuns.AccessProfile;
import br.com.datainfo.enuns.Situation;
import br.com.datainfo.utils.ConverterUtils;
import lombok.Data;

@Data
@Entity
@Table(name = "usuario_externo")
public class ExternalUser implements Serializable {

	private static final long serialVersionUID = 3956743345273684326L;

	@Id
	@Column(name = "nu_cpf", nullable = false)
	private String cpf;

	@Column(name = "no_usuario", nullable = false)
	private String name;

	@Column(name = "de_email", nullable = false)
	private String email;

	@Column(name = "ic_situacao", nullable = false)
	@Enumerated(EnumType.STRING)
	private Situation situation;

	@Column(name = "ic_perfil_acesso", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private AccessProfile accessProfile;

	@Column(name = "nu_telefone")
	private String phone;

	@OneToOne
	@JoinColumn(name = "co_funcao", referencedColumnName = "co_funcao")
	private ExternalUserFunction externalUserFunction;

	public ExternalUser() {
		this.situation = Situation.A;
	}

	public ExternalUserDto toDto() {
		return ConverterUtils.entityToDto(this);
	}

}
