package br.com.datainfo.enuns;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AccessProfile {

	STUDENT(0, "Aluno"), MUNICIPAL_MANAGER(1, "Gestor Municipal"), STATE_MANAGER(2, "Gestor Estadual"), NATIONAL_MANAGER(3, "Gestor Nacional");

	@Getter
	private Integer ordinal;
	@Getter
	private String description;

	AccessProfile(final Integer ordinal, final String description) {
		this.ordinal = ordinal;
		this.description = description;
	}

}
