package br.com.datainfo.enuns;

import lombok.Getter;

public enum Situation {

	A(Boolean.TRUE, "Ativo"), I(Boolean.FALSE, "Invativo");

	@Getter
	private Boolean status;
	@Getter
	private String description;

	Situation(final Boolean status, final String description) {
		this.status = status;
		this.description = description;
	}

}
